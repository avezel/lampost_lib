def register_editor_services():
    from lampost.editor import events
    events.register()


def add_common_routes():
    from lampost.editor import data, session
    from lampost.editor.players import PlayerEditor, UserEditor
    from lampost.server.link import add_link_module, add_link_object

    add_link_module(data)
    add_link_module(session)
    add_link_object('editor/player', PlayerEditor())
    add_link_object('editor/user', UserEditor())
