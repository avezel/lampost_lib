import yaml
import os

from lampost.db.registry import get_dbo_class
from lampost.di.config import config_value
from lampost.di.resource import Injected, module_inject

log = Injected('log')
db = Injected('datastore')
module_inject(__name__)


def yaml_import(file_name, clear_existing=False, file_path=None, **_):
    if file_path is None:
        file_path = config_value('import_path', '')
        if file_path:
            file_path += os.sep
    if '.' not in file_name:
        file_name += '.yaml'
    with open(file_path + file_name, 'r') as yf:
        log.info("Processing import file {}", file_name)
        yaml_load = yaml.load(yf)
    class_id = yaml_load['class_id']
    if clear_existing:
        dbo_class = get_dbo_class(class_id)
        parent_type = getattr(dbo_class, 'dbo_parent_type', None)
        if parent_type:
            parent_class = get_dbo_class(parent_type)
            for parent_key in db.fetch_set_keys(parent_class.dbo_set_key):
                child_set_key = '{}_{}s:{}'.format(parent_type, class_id, parent_key)
                for obj in db.load_object_set(class_id, child_set_key):
                    db.delete_object(obj)
        else:
            for obj in db.load_object_set(class_id):
                db.delete_object(obj)
    for dbo_id, dbo_data in yaml_load['data'].items():
        if clear_existing:
            db.create_object(class_id, dbo_data, dbo_id)
        else:
            existing = db.load_object(dbo_id, class_id, silent=True)
            if existing:
                existing.update(dbo_data)
            else:
                db.create_object(class_id, dbo_data, dbo_id)
