def enable_admin_ops():
    from . import dbops, imports


def add_admin_routes():
    from lampost.admin.ops import admin_operation
    from lampost.admin.imports import yaml_import
    from lampost.server.link import add_link_route

    add_link_route('admin/operations', admin_operation, 'supreme')
    add_link_route('admin/yaml_import', yaml_import)
