import time

import itertools

from lampost.admin.ops import admin_op
from lampost.db import dbconfig
from lampost.db.exceptions import DataError
from lampost.db.registry import get_dbo_class, _dbo_registry, get_mixed_type
from lampost.di.config import load_yaml, activate
from lampost.di.resource import Injected, module_inject

log = Injected('log')
db = Injected('datastore')
perm = Injected('perm')
module_inject(__name__)


@admin_op
def delete_all_objects(key_type, confirm="no"):

    execute = confirm == 'confirm'
    dbo_ids, _ = _class_data(key_type)

    deleted = 0
    for dbo_id in dbo_ids:
        dbo = db.load_object(dbo_id, key_type)
        if dbo:
            deleted += 1
            if execute:
                db.delete_object(dbo)
    return "{} objects of type {} {}deleted".format(deleted, key_type, '' if execute else 'would have been ')


@admin_op
def rebuild_indexes(key_type):
    dbo_ids, indexes = _class_data(key_type)
    for ix_name in indexes:
        db.delete_key('{}{}'.format(key_type, ix_name))
    updated = 0
    for dbo_id in dbo_ids:
        try:
            dbo = db.load_object(dbo_id, key_type)
            for ix_name in indexes:
                updated += 1
                db._add_to_index(key_type, ix_name, dbo_id, dbo.prop_value(ix_name[1:]))
        except (ValueError, TypeError):
            log.warn("Missing dbo object {} from set key {}", dbo_id, key_type)
    return "{} index values updated".format(updated)


@admin_op
def purge_invalid(confirm='no'):

    start_time = time.time()
    total = 0
    purged = 0

    execute = confirm == 'confirm'

    def purge(purge_cls, set_key=None):
        nonlocal purged, total

        for dbo_id in db.fetch_set_keys(set_key):
            total += 1
            dbo_key = ':'.join((purge_cls.dbo_key_type, dbo_id))
            dbo_dict = db.load_value(dbo_key)
            if dbo_dict is None:
                purged += 1
                log.warn("Missing value for key {}", dbo_key)
                if execute:
                    db.delete_set_key(set_key, dbo_id)
            else:
                dbo = get_mixed_type(purge_cls.dbo_key_type, dbo_dict.get('mixins'))()
                dbo.dbo_id = dbo_id
                if not dbo.hydrate(dbo_dict):
                    purged += 1
                    if execute:
                        db.delete_object(dbo)
                for child_type in getattr(dbo_cls, 'dbo_children_types', ()):
                    purge(get_dbo_class(child_type), '{}_{}s:{}'.format(dbo_key_type, child_type, dbo_id))

    for dbo_cls in _dbo_registry.values():
        dbo_key_type = getattr(dbo_cls, 'dbo_key_type', None)
        if dbo_key_type and not hasattr(dbo_cls, 'dbo_parent_type'):
            purge(dbo_cls, dbo_cls.dbo_set_key)

    return "{} of {} objects purged in {} seconds".format(purged, total, time.time() - start_time)


@admin_op
def rebuild_owner_refs():
    updated = 0
    rescued = 0
    for owned_key in db.redis.keys('owned:*'):
        db.delete_key(owned_key)
    for dbo_cls in _dbo_registry.values():
        dbo_key_type = getattr(dbo_cls, 'dbo_key_type', None)
        if not dbo_key_type:
            continue
        owner_field = dbo_cls.dbo_fields.get('owner_id', None)
        if not owner_field:
            continue
        for dbo in db.load_object_set(dbo_cls):
            if dbo.owner_id in perm.immortals:
                dbo.db_created()
                updated += 1
            else:
                log.warn("owner id {} not found, setting owner of {} to default {}", dbo.owner_id, dbo.dbo_id,
                         owner_field.default)
                dbo.change_owner()
                rescued += 1
    return "{} objects assigned to owners, {} objects assigned to system accounts".format(updated, rescued)


@admin_op
def rebuild_immortal_list():
    db.delete_key('immortals')
    for player in db.load_object_set('player'):
        if player.imm_level:
            db.index('immortals', player.dbo_id, player.imm_level)
    return "{} immortals registered".format(len(db.index_entries('immortals')))


@admin_op
def rebuild_all_fks():
    start_time = time.time()
    updated = 0

    def update(update_cls, set_key=None):
        nonlocal updated
        for dbo in db.load_object_set(update_cls, set_key):
            db.save_object(dbo)
            updated += 1
            for child_type in getattr(dbo_cls, 'dbo_children_types', ()):
                update(get_dbo_class(child_type), '{}_{}s:{}'.format(dbo_key_type, child_type, dbo.dbo_id))

    for holder_key in db.redis.keys('*:holders'):
        db.delete_key(holder_key)
    for ref_key in db.redis.keys('*:refs'):
        db.delete_key(ref_key)
    for dbo_cls in _dbo_registry.values():
        dbo_key_type = getattr(dbo_cls, 'dbo_key_type', None)
        if dbo_key_type and not hasattr(dbo_cls, 'dbo_parent_type'):
            update(dbo_cls)

    return "{} objects updated in {} seconds".format(updated, time.time() - start_time)


@admin_op
def restore_db_from_yaml(config_id='lampost', path='conf', force="no"):
    yaml_config = load_yaml(path)
    existing = db.load_object(config_id, 'config')
    if existing:
        if force != 'yes':
            return "Object exists and force is not 'yes'"
        db.delete_object(existing)
    try:
        db_config = dbconfig.create(config_id, yaml_config, True)
    except Exception as exp:
        log.exception("Failed to create configuration from yaml")
        db.save_object(existing)
        return "Exception creating configuration from yaml."
    activate(db_config.section_values)
    return 'Config {} successfully loaded from yaml files'.format(config_id)


def _class_data(key_type):
    dbo_cls = get_dbo_class(key_type)
    if not dbo_cls:
        raise DataError("Class not found")
    indexes, set_key, parent_type = dbo_cls.dbo_indexes, dbo_cls.dbo_set_key, getattr(dbo_cls, 'dbo_parent_type', None)

    if parent_type:
        parent_cls = get_dbo_class(parent_type)
        parent_ids = db.fetch_set_keys(parent_cls.dbo_set_key)
        dbo_ids = itertools.chain.from_iterable([db.fetch_set_keys("{}_{}s:{}".format(parent_type, key_type, parent_id)) for parent_id in parent_ids])
    elif set_key:
        dbo_ids = db.fetch_set_keys(set_key)
    else:
        raise DataError("Unrecognized class_id {}".format(key_type))

    return dbo_ids, indexes