from lampost.db.dbo import SystemDBO
from lampost.db.dbofield import AutoField, DBOField
from lampost.di.app import on_app_start
from lampost.di.config import on_config_change, config_value
from lampost.di.resource import Injected, module_inject
from lampost.event.attach import Attachable

log = Injected('log')
perm = Injected('perm')
ev = Injected('dispatcher')
module_inject(__name__)


@on_app_start
@on_config_change
def _config():
    global autosave_interval
    autosave_interval = config_value('autosave_interval', 0)


class User(SystemDBO):
    dbo_key_type = 'user'
    dbo_set_key = 'users'
    dbo_indexes = '@user_name', '@email'

    display_fields = 'dbo_id', 'user_name'

    user_name = DBOField('')
    password = DBOField()
    password_reset = DBOField(False)
    email = DBOField('')
    notes = DBOField('')

    player_ids = DBOField([])
    displays = DBOField({})
    notifies = DBOField([])

    @property
    def edit_dto(self):
        dto = super().edit_dto
        dto['password'] = ''
        return dto

    @property
    def imm_level(self):
        if self.player_ids:
            return max([perm.immortals.get(player_id, 0) for player_id in self.player_ids])
        return 0


class Player(SystemDBO, Attachable):
    dbo_key_type = 'player'
    dbo_set_key = 'players'
    dbo_indexes = '>user_id',

    display_fields = 'dbo_id', 'name', 'imm_level'

    session = AutoField()

    user_id = DBOField(0)
    created = DBOField(0)
    imm_level = DBOField(0)
    last_login = DBOField(0)
    last_logout = DBOField(0)
    age = DBOField(0)

    @property
    def edit_dto(self):
        dto = super().edit_dto
        dto['logged_in'] = "Yes" if self.session else "No"
        return dto

    @property
    def name(self):
        return self.dbo_id.capitalize()

    @property
    def location(self):
        return "Online" if self.session else "Offline"

    def _on_attach(self):
        if autosave_interval:
            ev.register_p(self.autosave, seconds=autosave_interval)
        self.active_channels = set()
        self.last_tell = None

    def _on_detach(self):
        self.session = None

    def check_logout(self):
        pass

    def display_line(self, text, display='default'):
        if text and self.session:
            self.session.append('display', {'text': text, 'display': display})

    def output(self, key, data):
        if self.session:
            self.session.update(key, data)

    def receive_broadcast(self, broadcast):
        self.display_line(broadcast.translate(self), broadcast.display)
