import time

from collections import deque
from weakref import WeakValueDictionary

from redis import ConnectionPool
from redis.client import StrictRedis

from lampost.di.resource import Injected, module_inject
from lampost.db.registry import get_dbo_class, get_mixed_type
from lampost.db.exceptions import ObjectExistsError, NonUniqueError
from lampost.util.classes import WeakList

log = Injected('log')
json_encode = Injected('json_encode')
json_decode = Injected('json_decode')
module_inject(__name__)


class RedisStore:
    def __init__(self, db_host, db_port, db_num, db_pw):
        self.pool = ConnectionPool(max_connections=2, db=db_num, host=db_host, port=db_port, password=db_pw,
                                   decode_responses=True)
        self.redis = StrictRedis(connection_pool=self.pool)
        self.redis.ping()
        self._object_map = WeakValueDictionary()
        self._index_map = WeakValueDictionary()

    def create_object(self, cls, dbo_dict, dbo_id=None):
        cls = get_dbo_class(getattr(cls, 'dbo_key_type', cls))
        dbo = cls()
        dbo.dbo_id = self._validate_id(cls.dbo_key_type, dbo_id or dbo_dict.get('dbo_id'))
        dbo.hydrate(dbo_dict)
        self._save_new_object(dbo)
        return dbo

    def insert_object(self, dbo):
        dbo.dbo_id = self._validate_id(dbo.dbo_key_type, dbo.dbo_id)
        self._save_new_object(dbo)
        return dbo

    def load_object(self, dbo_key, key_type=None, silent=False):
        if key_type:
            try:
                key_type = key_type.dbo_key_type
            except AttributeError:
                pass
            dbo_key, dbo_id = f'{key_type}:{dbo_key}', dbo_key
        else:
            key_type, _, dbo_id = dbo_key.partition(':')
        cached_dbo = self._object_map.get(dbo_key)
        if cached_dbo:
            return cached_dbo
        json_str = self.redis.get(dbo_key)
        if json_str is None:
            if not silent:
                log.warn("Failed to find {} in database", dbo_key)
            return
        return self._json_to_obj(json_str, key_type, dbo_id)

    def save_object(self, dbo, update_timestamp=False, autosave=False):
        if update_timestamp:
            dbo.dbo_ts = int(time.time())
        if dbo.dbo_indexes:
            self._update_indexes(dbo)
        self._clear_old_refs(dbo)
        save_root, new_refs = dbo.to_db_value()
        self.redis.set(dbo.dbo_key, json_encode(save_root))
        if new_refs:
            self._set_new_refs(dbo, new_refs)
        log.debug("db object {} {}saved", dbo.dbo_key, "auto" if autosave else "")
        self._object_map[dbo.dbo_key] = dbo
        return dbo

    def delete_object(self, dbo):
        key, dbo_id = dbo.dbo_key, dbo.dbo_id
        dbo.db_deleted()
        self.delete_key(key)
        self._clear_old_refs(dbo)
        if dbo.dbo_set_key:
            self.redis.srem(dbo.dbo_set_key, dbo_id)
        for children_type in dbo.dbo_children_types:
            self.delete_object_set(get_dbo_class(children_type),
                                   "{}_{}s:{}".format(dbo.dbo_key_type, children_type, dbo_id))
        for ix_name in dbo.dbo_indexes:
            self._remove_from_index(dbo.dbo_key_type, ix_name, dbo_id, dbo.prop_value(ix_name[1:]))
        log.debug("object deleted: {}", key)
        self.evict_object(dbo)

    def load_cached(self, dbo_key):
        return self._object_map.get(dbo_key)

    def object_exists(self, obj_type, obj_id):
        return self.redis.exists(f'{obj_type}:{obj_id}')

    def load_objects(self, key_type, dbo_ids):
        results = set()
        keys = deque()
        pipeline = self.redis.pipeline()
        for key in dbo_ids or ():
            dbo_key = ':'.join([key_type, key])
            try:
                results.add(self._object_map[dbo_key])
            except KeyError:
                keys.append(key)
                pipeline.get(dbo_key)
        for dbo_id, json_str in zip(keys, pipeline.execute()):
            if json_str:
                results.add(self._json_to_obj(json_str, key_type, dbo_id))
            else:
                log.warn(f"Requested missing object with key {key_type}:{dbo_id}")
        return results

    def load_object_set(self, dbo_class, set_key=None):
        dbo_class = get_dbo_class(getattr(dbo_class, 'dbo_key_type', dbo_class))
        key_type = dbo_class.dbo_key_type
        return self.load_objects(key_type, self.fetch_set_keys(set_key or dbo_class.dbo_set_key))

    def delete_object_set(self, dbo_class, set_key=None):
        if not set_key:
            set_key = dbo_class.dbo_set_key
        for dbo in self.load_object_set(dbo_class, set_key):
            self.delete_object(dbo)
        self.delete_key(set_key)

    def evict_object(self, dbo):
        self._object_map.pop(dbo.dbo_key, None)

    def load_value(self, key, default=None):
        json = self.redis.get(key)
        if json:
            return json_decode(json)
        return default

    def save_value(self, key, value):
        self.redis.set(key, json_encode(value))

    def fetch_set_keys(self, set_key):
        return self.redis.smembers(set_key)

    def add_set_key(self, set_key, *values):
        self.redis.sadd(set_key, *values)

    def delete_set_key(self, set_key, value):
        self.redis.srem(set_key, value)

    def set_key_exists(self, set_key, value):
        return self.redis.sismember(set_key, value)

    def db_counter(self, counter_id, inc=1):
        return self.redis.incr(f"counter:{counter_id}", inc)

    def delete_key(self, key):
        self.redis.delete(key)

    def index(self, index_name, key, value):
        return self.redis.hset(index_name, key, json_encode(value))

    def cached_index_values(self, index_name, key):
        cache_key = f'{index_name}${key}'
        try:
            return self._index_map[cache_key]
        except KeyError:
            values = self.index_value(index_name, key)
            values = WeakList(values) if values else WeakList()
            self._index_map[cache_key] = values
            return values

    def index_value(self, index_name, key):
        ix_value = self.redis.hget(index_name, key)
        if ix_value is not None:
            return json_decode(ix_value)

    def delete_index(self, index_name, key):
        return self.redis.hdel(index_name, key)

    def index_entries(self, index_name):
        return {key: json_decode(value) for key, value in self.redis.hgetall(index_name).items()}

    def index_values(self, hash_id):
        return [json_decode(value) for value in self.redis.hgetall(hash_id).values()]

    def get_db_list(self, list_id, start=0, end=-1):
        return [json_decode(value) for value in self.redis.lrange(list_id, start, end)]

    def add_db_list(self, list_id, value):
        self.redis.lpush(list_id, json_encode(value))

    def trim_db_list(self, list_id, start, end):
        return self.redis.ltrim(list_id, start, end)

    def dbo_holders(self, dbo_key, degrees=0):
        all_keys = set()

        def find(find_key, degree):
            holder_keys = self.fetch_set_keys('{}:holders'.format(find_key))
            for new_key in holder_keys:
                if new_key != dbo_key and new_key not in all_keys:
                    all_keys.add(new_key)
                    if degree < degrees:
                        find(new_key, degree + 1)

        find(dbo_key, 0)
        return all_keys

    def _save_new_object(self, dbo):
        dbo.db_created()
        self.save_object(dbo, True)
        if dbo.dbo_set_key:
            self.redis.sadd(dbo.dbo_set_key, dbo.dbo_id)

    def _validate_id(self, key_type, dbo_id):
        if dbo_id is None:
            new_id = str(self.db_counter(f'{key_type}_id'))
        else:
            new_id = str(dbo_id).lower()
        if self.object_exists(key_type, new_id):
            raise ObjectExistsError(new_id)
        return new_id

    def _json_to_obj(self, json_str, key_type, dbo_id):
        dbo_dict = json_decode(json_str)
        dbo = get_mixed_type(key_type, dbo_dict.get('mixins'))()
        dbo.dbo_id = dbo_id
        dbo.hydrate(dbo_dict)
        self._object_map[dbo.dbo_key] = dbo
        return dbo

    def _update_indexes(self, dbo):
        key_type, dbo_id, old_dbo, old_json = dbo.dbo_key_type, dbo.dbo_id, None, self.redis.get(dbo.dbo_key)
        if old_json:
            old_dict = json_decode(old_json)
            old_dbo = get_mixed_type(dbo.dbo_key_type, old_dict.get('mixins'))()
            old_dbo.hydrate(old_dict)

        for ix_name in dbo.dbo_indexes:
            old_val, new_val = old_dbo and old_dbo.prop_value(ix_name[1:]), dbo.prop_value(ix_name[1:])
            if old_val != new_val:
                self._remove_from_index(key_type, ix_name, dbo_id, old_val)
                self._add_to_index(key_type, ix_name, dbo_id, new_val)

    def _add_to_index(self, key_type, ix_name, dbo_id, value):
        ix_key = '{}{}'.format(key_type, ix_name)
        if ix_name[0] == '>':
            self.index(ix_key, dbo_id, value)
        elif ix_name[0] == '@' and value is not None and value != '':
            if self.index_value(ix_key, value):
                raise NonUniqueError(ix_key, value)
            self.index(ix_key, value, dbo_id)
        elif ix_name[0] == '$' and value is not None and value != '':
            ix_values = self.cached_index_values(ix_key, value)
            if dbo_id not in ix_values:
                ix_values.append(dbo_id)
            self.index(ix_key, value, ix_values)

    def _remove_from_index(self, key_type, ix_name, dbo_id, value):
        ix_key = '{}{}'.format(key_type, ix_name)
        if ix_name[0] == '>':
            self.delete_index(ix_key, dbo_id)
        elif ix_name[0] == '$' and value is not None and value != '':
            ix_values = self.cached_index_values(ix_key, value)
            if dbo_id in ix_values:
                ix_values.remove(dbo_id)
                if ix_values:
                    self.index(ix_key, value, ix_values)
                else:
                    self.delete_index(ix_key, value)
        elif ix_name[0] == '@' and value is not None and value != '':
            self.delete_index(ix_key, value)

    def _clear_old_refs(self, dbo):
        dbo_key = dbo.dbo_key
        ref_key = '{}:refs'.format(dbo_key)
        for ref_id in self.fetch_set_keys(ref_key):
            holder_key = '{}:holders'.format(ref_id)
            self.delete_set_key(holder_key, dbo_key)
        self.delete_key(ref_key)

    def _set_new_refs(self, dbo, new_refs):
        dbo_key = dbo.dbo_key
        self.add_set_key("{}:refs".format(dbo_key), *new_refs)
        for ref_id in new_refs:
            holder_key = '{}:holders'.format(ref_id)
            self.add_set_key(holder_key, dbo_key)
