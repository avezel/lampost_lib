import importlib
from lampost.di import resource


def cold_start(args, app_package, flavor_package=None, include_app_services=True, include_editor=True):
    from lampost.db.redisstore import RedisStore
    from lampost.di import config
    from lampost.db import dbconfig
    from lampost.util import json

    json.select_json()
    datastore = resource.register('datastore', RedisStore(args.db_host, args.db_port, args.db_num, args.db_pw))
    db_config = datastore.load_object(args.config_id, dbconfig.Config)
    config.activate(db_config.section_values)
    app_start(args, app_package, flavor_package, include_app_services, include_editor)


def app_start(args, app_package, flavor_package=None, include_app_services=True, include_editor=True):
    from lampost.db import permissions
    from lampost.event.system import dispatcher
    from lampost.user import manage as user_manager
    from lampost.server import web

    resource.register('dispatcher', dispatcher)
    resource.register('perm', permissions)
    resource.register('user_manager', user_manager)

    web.service_root = args.service_root

    if include_app_services:
        _start_app_services()

    if include_editor:
        from lampost.editor import register_editor_services
        register_editor_services()

    package_name = args.app_package or app_package
    app_module = importlib.import_module(package_name)
    if app_module:
        app_start = getattr(app_module, 'app_start')
        flavor_package = args.flavor_package or flavor_package
        app_start(args, flavor_package)

    from lampost.di import app
    app.bootstrap_app()
    _start_server(args, include_editor)


def _start_app_services():
    from lampost.admin import enable_admin_ops
    from lampost.server import session as session_manager
    from lampost.server.services import AnyLoginService, PlayerListService, EditUpdateService
    from lampost.service.channel import ChannelService
    from lampost.user import add_player_targets
    from lampost.gameops import friend
    from lampost.service import email as email_sender
    from lampost.service import message

    resource.register('session_manager', session_manager)
    resource.register('email_sender', email_sender)
    resource.register('channel_service', ChannelService())
    resource.register('friend_service', friend)
    resource.register('message_service', message)
    resource.register('player_list_service', PlayerListService())
    resource.register('login_notify_service', AnyLoginService())
    resource.register('edit_update_service', EditUpdateService())

    enable_admin_ops()
    add_player_targets()


def _start_server(args, include_editor):
    from tornado.ioloop import IOLoop

    from lampost.admin import add_admin_routes
    from lampost.util.logging import get_logger
    from lampost.server import services
    from lampost.server import web
    from lampost.server.link import add_link_module, add_link_route
    from lampost.user import settings
    from lampost.di.config import config_value

    add_link_route('register_service', services.register_service)
    add_link_route('unregister_service', services.unregister_service)
    add_link_module(settings)
    add_admin_routes()

    if include_editor:
        from lampost.editor import add_common_routes
        add_common_routes()

    tornado_logger = get_logger('tornado.general')
    tornado_logger.setLevel(config_value('tornado_log','WARN'))
    tornado_logger = get_logger('tornado.access')
    tornado_logger.setLevel(config_value('tornado_log', 'WARN'))

    web.start(args)

    IOLoop.instance().start()
