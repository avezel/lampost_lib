def config_logging(args):
    from lampost.util import logging
    logging.init_config(args)
    logging.root_logger.info("Started with args {}", args)
    log_factory = logging.LogFactory()

    from lampost.di import resource
    resource.register('log', log_factory)