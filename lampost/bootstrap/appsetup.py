import sys
import importlib


def setup_main(args, app_package=None, flavor_package=None):

    if args.flush and not args.no_confirm:
        response = input("Please confirm the database number you wish to clear: ")
        if response != str(args.db_num):
            print("Database numbers do not match")
            sys.exit(2)

    from lampost.util import json
    json.select_json()

    init_db(args)
    import_config(args)

    package_name = args.app_package or app_package
    first_player = None
    if app_package:
        app_module = importlib.import_module(package_name)
        app_setup = getattr(app_module, 'app_setup')
        flavor_package = args.flavor_package or flavor_package
        first_player = app_setup(args, flavor_package)
    create_root_user(args, first_player)


def init_db(args):
    from lampost.di import resource
    from lampost.db import redisstore
    db = resource.register('datastore', redisstore.RedisStore(args.db_host, args.db_port, args.db_num, args.db_pw))
    if args.flush:
        sys.stdout.write("Flushing database {}".format(args.db_num))
        db.redis.flushdb()


def import_config(args):
    from lampost.di import resource, config
    from lampost.db import dbconfig
    db = resource.get_resource('datastore')
    db_config = db.load_object(args.config_id, 'config')
    if db_config:
        raise RuntimeError("Error:  This instance is already set up")

    config_yaml = config.load_yaml(args.config_dir)
    db_config = dbconfig.create(args.config_id, config_yaml, True)
    config.activate(db_config.section_values)


def create_root_user(args, player_data):
    from lampost.di import resource
    from lampost.db import permissions
    from lampost.user.model import User
    from lampost.util.encrypt import make_hash

    db = resource.get_resource('datastore')
    user_dto = {'dbo_id': db.db_counter('user_id'),
                'user_name': args.imm_account,
                'password': make_hash(args.imm_password)}
    user = db.create_object(User, user_dto)
    if player_data:
        player_data['user_id'] = user.dbo_id
        player = db.create_object("player", player_data)
        user.player_ids.append(player.dbo_id)
        db.save_object(player)
        db.save_object(user)
        permissions.update_immortal_list(player)
