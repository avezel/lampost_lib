def display_dto(obj, observer=None):
    if not hasattr(obj, 'display_fields'):
        return obj
    display = {}
    for cls in obj.__class__.__mro__:
        for field_name in cls.__dict__.get('display_fields', ()):
            value = getattr(obj, field_name, None)
            if hasattr(value, '__call__'):
                value = value(observer)
            if value is None:
                display[field_name] = None
            elif isinstance(value, (int, str, bool, float, dict)):
                display[field_name] = value
            elif isinstance(value, (set, list, tuple)):
                display[field_name] = list(display_dto(member, observer) for member in value)
            elif isinstance(value, dict):
                display[field_name] = {key: display_dto(member) for key, member in value.items()}
            else:
                display[field_name] = display_dto(value, observer)
    return display
