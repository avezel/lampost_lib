import os

from tornado.httpserver import HTTPServer
from tornado.web import Application, URLSpec, RequestHandler, HTTPError, StaticFileHandler

from lampost.di.resource import Injected, module_inject
from lampost.server.link import LinkHandler

log = Injected('log')
module_inject(__name__)

service_root = '/'
_handlers = []


def start(args):
    add_raw_route('/link', LinkHandler)

    if args.web_files:
        log.info("Setting static handler to {}", args.web_files)
        add_raw_route('^[^.]+$', IndexHandler,
                      index_file=os.path.abspath(args.web_files) + os.path.sep + 'index.html')
        add_raw_route('/(.*)', StaticFileHandler, path=os.path.abspath(args.web_files))

    application = Application(_handlers, websocket_ping_interval=30)
    log.info("Starting web server on port {}", args.port)
    http_server = HTTPServer(application)
    http_server.listen(args.port, args.server_interface)


def add_route(url_regex, handler, **kwargs):
    add_raw_route(service_root + url_regex, handler, **kwargs)


def add_raw_route(url_regex, handler, **kwargs):
    _handlers.append(URLSpec(url_regex, handler, kwargs))


def add_routes(routes):
    for route in routes:
        try:
            add_route(route.url_regex, route.handler, **route.init_args)
        except AttributeError:
            add_route(*route)


class IndexHandler(RequestHandler):
    def initialize(self, index_file):
        self.index_file = index_file

    def get(self):
        self.set_header("Content-Type", 'text/html')
        self.set_header('Cache-control', 'private, no-cache, no-store, must-revalidate')
        self.set_header('Expires', '-1')
        self.set_header('Pragma', 'no-cache')
        if not os.path.exists(self.index_file):
            raise HTTPError(404)
        with open(self.index_file, "rb") as file:
            while True:
                chunk = file.read(64 * 1024)
                if chunk:
                    self.write(chunk)
                else:
                    return

    def data_received(self, chunk):
        pass
