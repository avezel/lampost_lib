## Lampost Library

A framework library initially developed to support the Lampost Mud.  Current components include:

### lampost.db

A fast and efficient storage mechanism for Python objects and their relationships, utilizing Redis for storage.
  
  * Database objects inherit from the CoreDBO class.  Persisted fields are identified by descriptor classes.  Full inheritance (including "mixins") is supported.
  
  * Database objects that are actually "in use" by the Lampost engine are further cached within the engine, using
    a WeakValueDictionary.  
  
  * Default values, including empty collections, are not persisted to improve storage usage and network efficiency.
  
  * Both unique and secondary indexes are created simply by adding the relevant field name into the dbo_indexes class collection.
    Indexes are implemented using Redis hashes.  Paired indexes can be used to implement many to many relationships.
   
  * Parent/child associations can be declaratively created between domain types.
  
  * Object templates can be used to generate template instances.  These instances can be properties of other database objects (including other
    template instances), and when persisted, only those values that differ from the template will actually be written to the database, again
    saving memory, file storage, and bandwith.
    
  * Object associations are automatically tracked in Redis.  This allows "live" updates to the objects even if they are
    loaded into the Lampost engine, since associated objects can be automatically reloaded to reflect changes.
    
  * Level and object "owner" permissions are available to support external editing of database objects.    
  ```
class User(ParentDBO):
    dbo_key_type = 'user'
    dbo_set_key = 'users'
    dbo_indexes = '@user_name', '@email', '$group'
    dbo_child_types = 'todo', 'task'
    
    user_name = DBOField('')
    password = DBOField()
    password_reset = DBOField(False)
    email = DBOField('', required=True)
    notes = DBOField([], dbo_class_id='note')
    group = DBOField(0)
  ```

  ```
  class TaskTemplate(ChildDBO, Template)
      dbo_key_type = 'task'
      dbo_parent_type = 'user'
    
      time_estimate = DBOField(0)
      description = DBOField('Do it')
  ```  

  ```
  class TaskInstance(TemplateInstance)
      template_id = 'task'
  
      percent_completed = DBOField(0)
  ```
### lampost.di
A simple Python dependency injection and dynamic configuration system

  * Singleton objects are registered with the di system on app initialization.  On application startup,
    these objects are injected into loaded modules
    
  * Configuration is stored as a Lampost DBO and its children in the lampost.db.  Config values can be read
    at runtime, or config change handlers can be registered to respond to configuration updates
    
  ```
  def start_core_services(args):
      resource.register('datastore', RedisStore(args.db_host, args.db_port, args.db_num, args.db_pw))
      ujson = importlib.import_module('ujson')
      resource.register('json_decode', ujson.decode)
      resource.register('json_encode', UJsonEncoder(ujson.encode).encode)  
  ```  
  ```
  db = Injected('datastore')
  email = Injected('email_sender')
  
  lampost_title = ConfigVal('lampost_title')
  
  def send_welcome_email():
      users = db.load_object_set('user')
      for user in users:
          email.send_email('{}, welcome to {}'.format(user.name, lampost_title.value), user.email) 
  ```


### lampost.event
A prioritized pub/sub event bus.

  * Events are plain old python objects.  Topics are just strings, and subscriptions are essentially callback functions.
  
  * Subscriptions can be assigned a priority to enforce ordering of event callbacks
  
  * The event bus includes a clock/pulse function, which allows objects to register to receive either a one time
    or repeating event at fixed points in engine "internal time"
 
### lampost.server
The Lampost server is built on Tornado web sockets

  *  Sessions are maintained for a fixed time period even after disconnect, and the
     lampost-ui library can re-establish a user session without forcing a new login
     
  *  The server includes a request/response to support aynsynchronous requests from
     the lampost-ui
     
  *  Client messages are automatically routed to the appropriate server handlers based on message paths
     
  *  Conversely, the server can publish messages to clients that are routed by lampost-ui
     event handlers


### License

The code is available under the [MIT license](LICENSE.txt).
